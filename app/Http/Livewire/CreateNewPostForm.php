<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CreateNewPostForm extends Component
{
    public function render()
    {
        return view('livewire.create-new-post-form');
    }
}
